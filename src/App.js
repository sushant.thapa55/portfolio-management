import { BrowserRouter, Route,Routes } from 'react-router-dom';
import './App.css';
import BuySell from './components/PAGES/BuySell';
import Dashboard from './components/PAGES/Dashboard';
import IndividualStock from './components/PAGES/IndividualStock';
import ListStocks from './components/PAGES/ListStocks';
import UserInfo from './components/PAGES/UserInfo';


function App() {
  return (
    < >
   <BrowserRouter>
    <Routes>

    <Route  path="/" element={<Dashboard />} />
    <Route  path="/list-all" element={<ListStocks />} />
    <Route path="/profit-loss" element={<BuySell />} />
    <Route path="/userinfo" element={<UserInfo />} />
    <Route path="/specific-stock" element={<IndividualStock />} />


    </Routes>



    


   </BrowserRouter>
 

    
    </>
  );
}

export default App;
