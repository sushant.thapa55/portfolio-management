import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

function ListStocks() {


    const [list, setList] = useState(['']);



    const getAPI = () => {
      fetch('http://localhost:5000/list')
    .then(response => response.json())
    .then((data) =>{
      setList(data);
      console.log(data);
      
    })
}



useEffect(()=>{
    getAPI();
},[]);


// const handleEvent = (() =>{
   
//     getAPI();
// })




  return (
    <div>
<table class="table">
  <thead>
    <tr>
      
      <th scope="col">Name</th>
      <th scope="col">Amount</th>
      <th scope="col">Quantity</th>
      <th scope="col">Date</th>
      <th scope="col">Type</th>
      <th scope="col">Total Investment</th>
      
     
    </tr>
  </thead>
  <tbody>
    

        {
            list.map((item) =>{
                return <tr>
            
                <td>{item.name}</td>
                <td>{item.Amount}</td>
                <td>{item.quantity}</td>
                <td>{item.trn_date}</td>
                <td>{item.trn_type}</td>
                <td>{item.Amount * item.quantity}</td>
                
              </tr>
            })
        }
       

       </tbody>
</table>


<Link to="/">back to dashboard.....</Link>
    </div>
  )
}

export default ListStocks