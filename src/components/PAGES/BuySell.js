import React, { useEffect, useState } from 'react';
import  TextField  from '@mui/material/TextField';
// import Autocomplete  from '@mui/material/Autocomplete';
import  Stack  from "@mui/material/Stack";
// import  Box  from "@mui/material/Box";
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { Autocomplete, CardContent, inputClasses } from '@mui/material';
import { Link } from 'react-router-dom';



function BuySell() {

    const [data,setData] = useState(['']);
    const [outputs,setOutput] = useState({
      percentageChange: "",
      amount_change:"",
      total_profit_loss:"",
      message:""
    });

  const [inputs,setInputs] = useState({
    name : "",
    no_of_stocks : "",
    current_price : "",
    purchase_price : "",
    date : "",
    purchase_sell : "",
  });

    const FetchAPI = (async () =>{
       await fetch('http://localhost:5000/list')
        .then(response =>response.json())
        .then((data) => {
            setData(data);
            // console.table(data);   
        })
    })

    // const [sel,setSel] = useState();
  
    //Filters the value
    // var res = data.filter((item) => {
    //   return item["Amount"] > 200  
    // })
   
    // console.log(res);

    
//  data.filter(function(amount){
//    return amount.Amount  
//  })   

//filter specific values

// let res = data.map(a => a.Amount);

    useEffect(()=>{
        FetchAPI();
        
    },[]);

   
    
   const handleChange=(e) =>{
    setInputs((prevState) => ({
      ...prevState,
      [e.target.name] : e.target.value
    }))

   };

   //handle submit

   const handleSubmit = (e) => {
     e.preventDefault();
  
   }
    
function checkProfitLoss() {
  const latestValue = Number(inputs.current_price);
  const oldValue = Number(inputs.purchase_price);
  const quantity = Number(inputs.no_of_stocks);


  if(latestValue > oldValue)
  {
    const profit = (latestValue-oldValue);
    const total_profit = (profit) * quantity;
    const profit_percentage = (profit/oldValue) * 100;
    
    setOutput({
     percentageChange : profit_percentage,
     amount_change : profit,
     total_profit_loss:total_profit,
     message:"Profit",

    });
  
    

  }
  else if (latestValue < oldValue){
    const loss = (oldValue-latestValue);
    const total_loss = (loss)*quantity;
    const loss_per = (loss/oldValue) * 100;
    setOutput({
      percentageChange : loss_per,
      amount_change: loss,
      total_profit_loss:total_loss,
      message:"Loss"
    });
  }

  else{
    setOutput({
      percentageChange: 0,
      profit : 0,
      loss:0
    })
  }


}


//handle Select

const handleSelect = (e) => {
  const get_val = e.target.value;
 
 return(inputs.name = get_val);
}
     
  
    return (

      <div>
   <Stack sx ={{ width:300, margin:"auto"}}>

     <select className="select-spa" name='name' value={inputs.name} onChange={handleSelect}>
      <option>--Select--</option>
      {
        data.map((item)=>{
          return <option  key={item.name} >{item.name}</option>
        })
      }
     </select>

     {/* <Autocomplete
     name="name"
     value={inputs.name} 
      options={data.map((item)=>{
        return <option value={item.name}>{item.name}</option>
      })}  
      onChange={handleChange}
      
      id="combo-box-demo"
    
    
      sx={{ width: 300 }}
      renderInput={(params) => <TextField {...params} label="Stocks" />}
    /> */}


<TextField onChange={handleChange} id="standard-basic" name="no_of_stocks" value={inputs.no_of_stocks} label="No of Stocks"  sx={{marginTop:"15px"}}/>
<TextField onChange={handleChange} id="standard-basic" name="current_price" value={inputs.current_price} label="Current Price per Unit"  sx={{marginTop:"15px"}} />
<TextField onChange={handleChange} id="standard-basic" name="purchase_price" value={inputs.purchase_price} label="Purchase/Sold Price per Unit"  sx={{marginTop:"15px"}} />

<TextField
   
    onChange={handleChange}
    name="date"
    value={inputs.date}
    sx={{marginTop:"15px"}}
    id="date"
    label="Transaction Date"
    type="date"
    defaultValue="2017-05-24"
    InputLabelProps={{
      shrink: true,
    }}
  />

<div style={{marginRight:"10px"}}>
<input onChange={handleChange}  type="radio" name="purchase_sell" id="rdbtn" value="buy" /> 
  <span>Buy</span>

<input onChange={handleChange} type="radio" name="purchase_sell" id="rdbtn" value="sell"/> 
  <span>Sell</span>
</div>


<button type='button' onClick={handleSubmit,checkProfitLoss}>submit</button>


   </Stack>
<br/>

<Card sx={{ minWidth: 500  }}>
<CardContent>

<Typography sx={{fontSize:14}} color="GrayText.primary">Name {inputs.name}</Typography>
</CardContent>

  <CardContent>

    <Typography sx={{fontSize:14}} color="GrayText.primary">Total {outputs.message} Per Share :  {outputs.amount_change}</Typography>
  </CardContent>
  <CardContent>

<Typography sx={{fontSize:14}} color="GrayText.primary">Total {outputs.message}Percentage : {outputs.percentageChange}</Typography>
</CardContent>
<CardContent>

    <Typography sx={{fontSize:14}} color="GrayText.primary">Total {outputs.message} Amount: {outputs.total_profit_loss}</Typography>
  </CardContent>


</Card>

<Link to="/"> back to dashboard.....</Link>
   </div>
  )
}

export default BuySell