import './IndividualStock.css'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

function IndividualStock() {


const [data,setData] = useState(['']);
const [selval,setSelval] = useState(null);
// const [inputs,setInput] = useState({
//     name:"",
//     quantity:"",
//     price: "",
//     date:"",
//     type:""

// });


const getAPI = (()=> {
    fetch("http://localhost:5000/list")
    .then(response => response.json())
    .then(data => {
        setData(data);
    })
})

useEffect(()=>{
    getAPI();
},[]);

const handleSelectChange=(event) =>{
    
    setSelval(event.target.value);

}

const obj = data.filter((x) => x.name === selval )
console.log(obj);
console.log(obj.Amount);





  return (
      <>    <div><h3>Yeah!!!Lets Check Stock Individually,</h3></div>
  
    <div className='individual-container'>
       
        <div>
            <select value={selval} onChange={handleSelectChange} className="select-space">
                <option>--select--</option>
            {
                data.map((item) => {
                     return  <option value={item.name}>{item.name}</option> 
                })



            }

            </select>
        </div>
        {/* <div>
            <button onClick={handleEvent}>Show info</button>
        </div> */}

    </div>

  
    <div  className='info-stock'>
        {obj.map((item) => {
            return <>
            <h3>Name:{item.name}</h3>
            <h3>Price Per Stock:{item.Amount}</h3>
            <h3>Total No. Of Stock Present:{item.quantity}</h3>
            <h3>Type :{item.trn_type}</h3>
            <h3>Transation Date :{item.trn_date}</h3>
            </>
        })}
    </div>



    <div className="back-link">
        <Link to="/">back to dashboard.....</Link>
        
    </div>
  </>

  )
}

export default IndividualStock