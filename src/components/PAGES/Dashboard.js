import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import './Dashboard.css'
function Dashboard() {

  const [data,setData] = useState(['']);


  const fetchdata = (() => {

    fetch("http://localhost:5000/list")
    .then(response => response.json())
    .then((data) => {
      setData(data);

    })
  });


  useEffect(()=>
  {
    fetchdata()

  },[]);

//calculating total amount invested 
  var amount_array = data.map(x =>x.Amount*x.quantity)
  var total_amount=amount_array.reduce((a,b) => a+b,0)


//total no of stock present in portfolio
var arr_stock = data.map(x=> x.quantity)
var total_stock = arr_stock.reduce((a,b)=> a+b,0)

//calculate total no of stock purchased

  var pur_array = data.filter(x =>{
    
        return x.trn_type === 'Buy';


  })

  var total_pur = pur_array.map(x =>x.Amount* x.quantity)
  var total_pur_amount = total_pur.reduce((a,b) => a+b,0)


  var sell_array = data.filter((x) =>{
    return x.trn_type ==='Sell';
  })


  var sel_list = sell_array.map(x=>x.Amount * x.quantity)
  var arr_sold = sell_array.map(x => x.quantity)

  var total_stock_sold = arr_sold.reduce((a,b) => a+b,0)
  var total_sold_amount = sel_list.reduce((a,b) => a+b,0)
 
  // var pur = pur_array.reduce((a,b) => a+b,0)
  // var sel = sell_array.reduce((a,b) => a+b,0)
  
  

  return (
   <>
   <h1 className='logo-main'>Portfolio Management </h1>
   <div className='user-info'><Link to="/userinfo"><img className='info-img' src='/images/hari.jpeg' alt='img' /> <h3 className='user-name'>user info</h3></Link></div>

<div className='main-container'>
    <div className='web-links-first'>
      <Link to="/list-all">List All Stocks</Link>
    
    </div>
    <div className='web-links-second'>
      <Link to="/profit-loss">Sell/Buy</Link>
    
    </div>
    <div className='web-links-third'>
      <Link to="/specific-stock">Check Spec Stock</Link>
    
    </div>
    <div className='web-links-fourth'>
    <Link to="/list-all">Total Loss</Link> 

   
  
</div>
</div>
    <div className='list-container'>
      <div className='inner-text'>
    <h3>Total Amount Invested : {total_amount}</h3>
    <h3>Total No. of Stocks :{total_stock}</h3>
    <h3>Total No. of Stocks Sold :{total_stock_sold}</h3>
    <h3>Total Worth of Stocks Rem : {total_pur_amount}</h3>
    <h3>Total Worth of Stock Sold :{total_sold_amount} </h3>
    {/* <h3>Total Sold Stock : {sel}</h3> */}
    </div>
    </div>
   
   </>
  )
}

export default Dashboard